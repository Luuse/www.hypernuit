<?php include('includes/head.php') ?>

<?php foreach($page->children as $article):?>
<article class="item <?=$page->name?>">
		<?php
		if($article->photo->first){
			$thumb = $article->photo->first->size(400);?>
			<img id="first-image" src="<?=$thumb->url?>" />
		<?php } ?>
		<a href="<?=$article->name?>">
			<h1> <?=$article->title?> </h1>
				<!-- <div class="article-date"> <?=$article->date_fin; ?> </div>-->
		</a>
	</article>

<?php endforeach; ?>

<?php include('includes/foot.php') ?>
