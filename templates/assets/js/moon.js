var moons = []
moons['nM'] = '🌑︎'
moons['wxCM'] = '🌒︎'
moons['qM'] = '🌓︎'
moons['wxGM'] = '🌔︎'
moons['fM'] = '🌕︎'
moons['wnGM'] = '🌖︎'
moons['lQM'] = '🌗︎'
moons['wnCM'] = '🌘︎'


Date.prototype.addDays = function(days) {
	var dat = new Date(this.valueOf())
	dat.setDate(dat.getDate() + days)
	return dat
}

function getDates(startDate, stopDate) {
	var dateArray = new Array()
	var currentDate = startDate
	while (currentDate <= stopDate) {
		dateArray.push(currentDate)
		currentDate = currentDate.addDays(1)
	}
	return dateArray
}


function getDateInterval(startDate, endDate, containerID) {
	var dateArray = getDates(new Date(startDate), new Date(endDate))
	containerID.innerHTML = ""
	var old = ''

	for (i = 0; i < dateArray.length; i ++ ) {
		var m = moon_phase(dateArray[i])
		if (m !== 'false') {
			var div = document.createElement('DIV')
			div.className = m
			div.setAttribute('data-t', dateArray[i].toDateString())
			div.setAttribute('data-m', moons[m])
			// containerID.innerHTML += '<div title="'+dateArray[i].toDateString()+' - '+m+'" class="day '+m+'"></div>'
			containerID.innerHTML += div.outerHTML
			// containerID.innerHTML += m
		}
		old = m
	}

	var DIVS = document.querySelectorAll('#moon-timeline > div')
	DIVS.forEach(function(item, i){
		item.addEventListener('mouseover', function(){
			var Top = item.getBoundingClientRect().top	
			_MOON_DATE.innerHTML = this.getAttribute('data-t') + ' <span>'+this.getAttribute('data-m')+'</span>'
			_MOON_DATE.style.top = Top+'px'
		})
		item.addEventListener('mouseleave', function(){
			_MOON_DATE.innerHTML = '' 
		})
	})
}


function moon_phase(date) {
		var year = date.getFullYear();
		var month = date.getMonth()-1; //index 0-11 check 
		var day = date.getDate();

    if (month < 3) {
        year--;
        month += 12;
    }

    ++month;

    jd = 365.25 * year + 30.6 * month + day - 694039.09; // jd is total days elapsed
    jd /= 29.53; // divide by the moon cycle (29.53 days)
    phase = parseInt(jd, 10); // int(jd) -> phase, take integer part of jd
    jd -= phase; // subtract integer part to leave fractional part of original jd
    phase = Math.ceil(jd * 8); // scale fraction from 0-8 and round by adding 0.5
    phase = phase & 7; // 0 and 8 are the same so turn 8 into 0

    switch (phase) {
        case 0: phase = "nM"; break;
        // case 0: phase = "new_Moon"; break;
        case 1: phase = "wxCM"; break;
        // case 1: phase = "waxing_Crescent_Moon"; break;
        case 2: phase = "qM"; break;
        // case 2: phase = "quarter_Moon"; break;
        case 3: phase = "wxGM"; break;
        // case 3: phase = "waxing_Gibbous_Moon"; break;
        case 4: phase = "fM"; break;
        // case 4: phase = "full_Moon"; break;
        case 5: phase = "wnGM"; break;
        // case 5: phase = "waning_Gibbous_Moon"; break;
        case 6: phase = "lQM";
        // case 6: phase = "last_Quarter_Moon";
        case 7: phase = "wnCM"; break;
        // case 7: phase = "waning_Crescent_Moon"; break;
    }
    return phase;
}

function allMoons(){
	let startDate = document.getElementById('start').value
	let endDate = document.getElementById('end').value
	getDateInterval(startDate, endDate)
}



