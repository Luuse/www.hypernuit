let _HOME_ARTICLES
let _ABOUT 
let _NAV_BUTTONS
let _MAIN
let _MOON_TL
let _MOON_DATE
let _HOME_ARTICLE_TITLE
let _HOME_URL
let _NAV_RESP

document.addEventListener('DOMContentLoaded', function(){
	_HOME_ARTICLES = document.querySelectorAll('article.item')
	film =  document.querySelectorAll('article.item.films-installations')
	_HOME_ARTICLE_TITLE = document.querySelector('.home_article_title')
	_NAV_BUTTONS = document.querySelectorAll('li.nav-part')
	_MAIN = document.querySelector('main')
	_MOON_TL = document.getElementById('moon-timeline')
	_ABOUT = document.getElementById('about')
	_MOON_DATE = document.getElementById('moon-date')
	_HOME_URL = document.body.getAttribute('data-home-url')
	_NAV_RESP = document.querySelectorAll('.part-resp')[0]

	if (window.location.hash){
		_ABOUT.classList.remove('block')
	} else if(window.location.search !== '?about=false'){
		_ABOUT.classList.add('block')
	}

	if(window.location.hash != '') {
		var cat = window.location.hash.replace('#','');
		if (['films-installations', 'ateliers', 'sounds', 'screenings', 'peoples'].includes(cat)){
			if(_MAIN.classList.contains('home')) {
				sortCategory(cat)
			}
		}

		setTimeout(function(){
			_MAIN.classList.add('visible')
		}, 500)

	}else{
		sortPublished()
	  _MAIN.classList.add('visible')
	}

	var today = new Date; 
	var favicon = document.getElementById('favicon') 
	favicon.href = favicon.href+moon_phase(today)+'.png'



	// M O O N  !!

	if(_MAIN.classList.contains('home')) {

			var size_wind = window.innerWidth > 1040 ? true : false
			window.addEventListener("resize", function(){
				if(window.innerWidth > 1040 && size_wind == false){
					window.location.reload()
				  size = true
				} 
			})



		_HOME_ARTICLES.forEach(function(item, i){
			if (window.innerWidth > 1040){
				if(item.classList.contains("peoples")){
					flyingPeople(item, i)
				} else if (item.classList.contains("sounds")){
					flyingSound(item, i)
				} else if (item.classList.contains("films-installations")){
					flyingFilminstall(item, i)
				} else if (item.classList.contains("ateliers")){
					flyingAtelier(item, i)
				} else if (item.classList.contains("screenings")){
					flyingScreening(item, i)
				} else {
					flying(item, i)
				}
			}




			item.addEventListener('mouseover', function(){
				var image = this.getAttribute('data-image')
				var cat = this.getAttribute('data-category')
				var title = this.querySelector('H1').innerHTML
					_HOME_ARTICLE_TITLE.innerHTML = ''
				if (image){
					_HOME_ARTICLE_TITLE.innerHTML = '<div class="filtered '+cat+'"><img src="'+image+'" /></div>'
				}
				_HOME_ARTICLE_TITLE.innerHTML += '<div class="title '+cat+'"><div class="category">'+cat.replace('peoples', 'people')+'</div><br><div class="sticky">'+title+'</div></div>'
			})
			item.addEventListener('mouseleave', function(){
				_HOME_ARTICLE_TITLE.innerHTML = ''
			})
		})

	}else{
		slider()
		pdf_viewer()
		
		if (window.innerWidth > 1040){
			var item = document.getElementsByTagName('article')[0]
			let cat = item.getAttribute('data-category')
			_MOON_TL.setAttribute('data-cat', cat)
			let startDate = item.getAttribute('data-date-start')
			let endDate = item.getAttribute('data-date-end')
			let title = document.querySelectorAll('.home_article_title .sticky')[0]
			setTimeout(function(){
				getDateInterval(startDate, endDate, _MOON_TL)
				_MOON_TL.classList.add('transition')
			}, 1000)
		}
		if (_MAIN.classList.contains('sound')) {
			player()
		}
	}

	if(_MAIN.classList.contains('atelier')) {
		scrollH()
	}

})
