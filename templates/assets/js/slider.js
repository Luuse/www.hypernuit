var _IMAGES
var _SLIDER
var _VIEWER_PDF


function build_slider(i){
	var IMG = document.createElement('img')
	IMG.src = _IMAGES[i].getAttribute('data-hight')
	IMG.id = 'slider_img'

	var DES = document.createElement('div')
	DES.innerHTML = _IMAGES[i].getAttribute('alt')
	DES.id = 'img_description'

	var BOX = document.createElement('div')
	BOX.id = 'slider_box'

	var CLOSE = document.createElement('div')
	CLOSE.id = 'slider_close'
	CLOSE.innerHTML = '✖'

	var NEXT = document.createElement('div')
	NEXT.id = 'slider_next'

	var PREV = document.createElement('div')
	PREV.id = 'slider_prev'

	BOX.innerHTML = NEXT.outerHTML
	BOX.innerHTML += PREV.outerHTML
	BOX.innerHTML += IMG.outerHTML
	BOX.innerHTML += CLOSE.outerHTML
	BOX.innerHTML += DES.outerHTML
	_SLIDER.innerHTML = BOX.outerHTML

	var slider_IMG = document.getElementById('slider_img')
	var slider_NEXT = document.getElementById('slider_next')
	var slider_PREV = document.getElementById('slider_prev')
	var slider_CLOSE = document.getElementById('slider_close')
	var slider_DES = document.getElementById('img_description')
	// var _BODY = document.querySelector('BODY')

	var e_close = function(){
		_SLIDER.innerHTML = ''
	}
	var e_next = function(){
		i == (_IMAGES.length - 1) ? i = 0: i++
		slider_IMG.src = _IMAGES[i].getAttribute('data-hight')
		slider_DES.innerHTML = _IMAGES[i].getAttribute('alt')
	}
	var e_prev = function(){
		i == 0 ? i = (_IMAGES.length - 1): i--
		slider_IMG.src = _IMAGES[i].getAttribute('data-hight')
		slider_DES.innerHTML = _IMAGES[i].getAttribute('alt')
	}

	slider_NEXT.addEventListener('click', e_next)
	slider_PREV.addEventListener('click', e_prev)
	slider_CLOSE.addEventListener('click', e_close)
	// _BODY.addEventListener('click', e_close)
}

function slider(){
	_IMAGES = document.querySelectorAll('.gallerie img, .gallerie-resp img')
	_SLIDER = document.getElementById('slider')
	_IMAGES.forEach(function(item, i){
		item.addEventListener('click', function(){
			build_slider(i)

		})
	})
}

function build_pdf_viewer(url){
	let iframe = document.createElement('IFRAME')
	iframe.src = url
	var CLOSE = document.createElement('div')
	CLOSE.id = 'slider_close'
	CLOSE.innerHTML = '✖'
	_VIEWER_PDF.innerHTML = iframe.outerHTML
	_VIEWER_PDF.innerHTML += CLOSE.outerHTML
	var slider_CLOSE = document.getElementById('slider_close')
	var e_close = function(){
		_VIEWER_PDF.innerHTML = ''
	}
	slider_CLOSE.addEventListener('click', e_close)
}

function pdf_viewer(){
	_PDFS = document.querySelectorAll('.file_pdf')
	_VIEWER_PDF = document.getElementById('viewer_pdf')
	_PDFS.forEach(function(item, i){
		item.addEventListener('click', function(){
			_SLIDER.innerHTML = ''
			var url = item.getAttribute('data-url')
			build_pdf_viewer(url)
		})
	})
}
