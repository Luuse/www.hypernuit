function rdm(max) {
  return Math.floor(Math.random() * max);
}

function closeElem(elm) {
	var element = document.querySelector(elm)
	element.classList.remove('visible')
	element.classList.add('hiden')
	setTimeout(function(){
		element.classList.add('none')
	}, 500)
}

function closeAbout() {
	// _ABOUT.classList.remove('visible')
	if(window.location.search != '?about=false') {
		window.location.hash = '#about-false'
	}
	_ABOUT.classList.add('hiden')
	_ABOUT.classList.add('none')
	setTimeout(function(){
		_ABOUT.classList.remove('block')
	}, 500)
}

function openAbout() {
	_ABOUT.classList.remove('hiden')
	_ABOUT.classList.remove('none')
	setTimeout(function(){
		_ABOUT.classList.add('block')
	}, 500)

}

function sortPublished(){
	var l =  _HOME_ARTICLES.length
	var articles = []

	for (var i = 0; i < l; ++i) {
		articles.push(_HOME_ARTICLES[i])
		_HOME_ARTICLES[i].classList.remove('.visible')
	}

	articles.sort(function(a, b) {
		return b.dataset.published - a.dataset.published
	});

	for (var i = 0; i < articles.length; ++i) {
		articles[i].parentElement.style.order = i
		articles[i].classList.add('visible')
	}
}

function sortCategory(cat){
	if (window.innerWidth < 1040){
		_NAV_RESP.removeAttribute('open')
	}
	function swtch(cat) {
		_HOME_ARTICLES.forEach(function(item, i){
			if(item.getAttribute('data-category') == cat) {
				item.classList.remove('hiden')
				item.classList.remove('none')
				item.classList.add('visible')
			}else{
				item.classList.add('none')
			}
		})
		let _FOOT_RESP = document.getElementById('foot-resp')
		_FOOT_RESP.innerHTML = cat.replace('-',' ').replace('peoples', 'people')
		_FOOT_RESP.classList.add('visible')
	}

	_NAV_BUTTONS.forEach(function(item, i){
		item.classList.remove('active')
	})

	_HOME_ARTICLES.forEach(function(item,i){
		item.classList.add('hiden')
		item.classList.remove('visible')
	})

	if(window.location.hash != '' && _MAIN.classList.contains('home')) {
		window.location.hash = cat
		var cat = window.location.hash.replace('#','');
		if (['films-installations', 'ateliers', 'sounds', 'screenings', 'peoples'].includes(cat)){
			if(_MAIN.classList.contains('home')) {
				document.location.href = '#'+cat;
				setTimeout(function(){
					swtch(cat)
				}, 500)
			}
			if (cat == 'peoples'){ _MAIN.classList.add('home-peoples') }
			else {_MAIN.classList.remove('home-peoples')}
		}
	}else {
		if (['films-installations', 'ateliers', 'sounds', 'screenings', 'peoples'].includes(cat)){
			if(_MAIN.classList.contains('home')) {
				if (cat == 'peoples'){ _MAIN.classList.add('home-peoples') }
				else {_MAIN.classList.remove('home-peoples')}
				document.location.href = '#'+cat;
				setTimeout(function(){
					swtch(cat)
				}, 500)
			}else {
				document.location.href = _HOME_URL+'?about=false#'+cat;
			}
		}
	}

}


function scrollH() {
	let images = document.querySelector('.gallerie .images')
	let right = document.querySelector('.gallerie .scroll_right')
	let left = document.querySelector('.gallerie .scroll_left' )

	var interv = 2
	var pos = 0

	left.addEventListener('mouseover', function(){
		var moove = function(){
			if(pos < 0) {
				pos =  pos + interv
				images.style.marginLeft = pos + 'px';
			}
			console.log(images.style.marginLeft)
			console.log(pos)
			console.log('----')
		}
		var action = setInterval(moove, 10)
		left.addEventListener('mouseout', function(){
			clearInterval(action, 800)
		})

	})
	right.addEventListener('mouseover', function(){
		var moove = function(){
			if(pos > ((images.offsetWidth - window.innerWidth) * -1)) {
				pos = pos - interv
				images.style.marginLeft = pos + 'px';
			}
			console.log(images.style.marginLeft)
			console.log(pos)
			console.log('----')
		}
		var action = setInterval(moove, 10)
		right.addEventListener('mouseout', function(){
			clearInterval(action, 800)
		})

	})




}


function flying(item, i) {
	item.style.marginTop = ''+0*i+'px';
	item.style.transform = 'rotate('+55*i+'deg) scale(1.5)';
}

function flyingScreening(item, i) {
	item.style.marginTop = ''+10*i+'px';
	setTimeout(function(){
		item.classList.add('anime')
	}, rdm(2000));
	// item.style.transform = 'rotate('+55*i+'deg) scale(1.5)';
}

function flyingPeople(item, i) {
	item.style.marginTop = ''+10*i+'px';
	item.style.marginLeft = '-'+4*i+'px';
	setTimeout(function(){
		item.classList.add('anime')
	}, rdm(1000));
	// item.style.transform = 'scale(1)';
	// item.style.transform = 'translate(0px, '+8*i+'px) scale(1)';
}

function flyingSound(item, i) {
	item.style.marginTop = ''+0*i+'px';
	setTimeout(function(){
		item.classList.add('anime')
	}, rdm(2000));
	// item.style.transform = 'scale(2)';
}

function flyingFilminstall(item, i) {
	item.style.marginTop = ''+55*i+'px';
	setTimeout(function(){
		item.classList.add('anime')
	}, rdm(3000));
	// item.style.transform = 'scale(1)';
}

function flyingAtelier(item, i) {
	item.style.marginTop = ''+5*i+'px';
	item.style.transform = 'rotate('+10*i+'deg) scale(1.5)';
	setTimeout(function(){
		item.classList.add('anime')
	}, rdm(1000));
}
