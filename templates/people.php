<?php include('includes/head.php'); ?>
<article class="people" data-date-start="<?=$page->date_start?>" data-date-end="<?=$page->date_end?>" data-category="<?=$page->parent->name?>">
			<div class="col_right-resp">
					<div class="intro-resp">
						<?php $peoples_page = $pages->get('template=peoples');?>
						<?php echo $peoples_page->text; ?>
					</div>
			</div>
	<div class="home_article_title">
		<div class="<?=$page->parent->name?>">
		<div class="sticky"><?=$page->title?></div>
		</div>
	</div>

	<div class="content">
		<div class="texte">
			<!--<div class="info_technique">People <?=$page->info_technique?>, <?=$page->date_end?></div>-->
				<?php  foreach($page->photo as $image) {
				$thumb = $image->size(300); ?>
				<div class="gallerie">
				<img src='<?=$thumb->url?>' data-hight="<?=$image->url?>" alt='<?=$image->description?>'/>
				</div>
				<div class="gallerie-resp">
				<img src='<?=$thumb->url?>' data-hight="<?=$image->url?>" alt='<?=$image->description?>'/>
				</div>
				<?php } ?>

			<?php if($page->text_fr != ''){ ?>
			<div class="intention">
				<span class="subtitle">FR :</span>
				<span><?=$page->text_fr?></span>
			</div>
			<?php } ?>
			<?php if($page->text_en != ''){ ?>
			<div class="intention">
				<span class="subtitle">EN :</span>
				<span> <?=$page->text_en?> </span>
			</div>
			<?php } ?>
			<?php if($page->text_it != ''){ ?>
			<div class="intention">
				<span class="subtitle">IT :</span>
				<span> <?=$page->text_it?> </span>
			</div>
			<?php } ?>
			<?php if($page->credits_libre != ''){ ?>
			<div class="credits_libre credits">
				<?=$page->credits_libre?>
			</div>
			<?php } ?>
			<?php if(
					 $page->ref_ateliers[0] != '' ||
					 $page->ref_sounds[0] != '' ||
					 $page->rel_films_install[0] != ''
				){ ?>
			<div class="relation">
				<u class="subtitle">RELATED PROJECTS :</u>
				<?php if ($page->rel_films_install[0]) { ?>
				<?php  foreach($page->rel_films_install as $film) { ?>
					<a href="<?=$film->url?>/?about=false" class="films-installations">
						<div class="nom"> &nbsp;<?=$film->title?></div>
					</a>
				<?php	} ?>
			<?php  } ?>

				<?php if ($page->ref_ateliers[0]) { ?>
				<?php  foreach($page->ref_ateliers as $atelier) { ?>
					<a href="<?=$atelier->url?>/?about=false" class="atelier">
						<div class="nom"> &nbsp;<?=$atelier->title?></div>
					</a>
				<?php	} ?>
			<?php  } ?>
			<?php if ($page->ref_sounds[0]) { ?>
				<?php  foreach($page->ref_sounds as $sound) { ?>
					<a href="<?=$sound->url?>/?about=false" class="sound">
						<div class="nom"> &nbsp;<?=$sound->title?></div>
					</a>
				<?php	} ?>
			<?php  } ?>
			</div>
			<?php  } ?>
			<div class="col_right-resp">
				<div class="peoples credits ">
					<?php  $peoples = $pages->find('template=people, sort=name');
					foreach ($peoples as $people) {
						if ($people->part !== 'people' && ($people->text_fr !== ''
							|| $people->text_en !== ''
							|| $people->text_it !== ''
						)) {

					?>
					<a href="<?=$people->url?>/?about=false" class="personne">
						<div class="nom">
							<?=$people->title?></br>
						</div>
					</a>
					<?php } } ?>

				</div>
			</div>
		</div>

			<div class="col_right">
					<?php $peoples_page = $pages->get('template=peoples');?>
					<?php echo $peoples_page->text; ?>
				<div class="peoples credits ">
					<?php  $peoples = $pages->find('template=people, sort=name');
					foreach ($peoples as $people) {
						if ($people->part !== 'people' && ($people->text_fr !== ''
							|| $people->text_en !== ''
							|| $people->text_it !== ''
						)) {

					?>
					<a href="<?=$people->url?>/?about=false" class="personne">
						<div class="nom">
							<?=$people->title?></br>
						</div>
					</a>
					<?php } } ?>

				</div>
			</div>





	</div>



	</div>



</article>

<?php include('includes/foot.php'); ?>
