<?php include('includes/head.php') ?>
<div id="slider"></div>
<div id="viewer_pdf"></div>
<div class="intro">
<div class="intentions"><?=$page->text_en?></div>
</div>

<?php foreach($page->children as $article):?>
<article class="<?=$page->name?> <?php if($article->coming_soon == 0){echo 'archive';} ?>" id="<?=$article->name?>">
			<div class="article-header">
				<h1><?=$article->title?> </h1>
			</div>
		<div class="content">
		<div class="col_left">
		<div class="gallerie">
			<?php  foreach($article->photo as $image) {
			$thumb = $image->size(500); ?>
			<img src='<?=$thumb->url?>' data-hight="<?=$image->url?>" alt='<?=$image->description?>'/><?php } ?>
		</div>
		</div>
		<div class="texte">
			<div class="info_pratique">
				<div class="gallerie-resp">
					<?php  foreach($article->photo as $image) {
					$thumb = $image->size(500); ?>
					<img src='<?=$thumb->url?>' data-hight="<?=$image->url?>" alt='<?=$image->description?>'/><?php } ?>
				</div>
				<div class="article-date">
					<?php if($article->coming_soon == 1){ ?><span class="coming_soon">Coming Soon</span> <?php } ?>
					<?php echo strftime("%d %B %Y", strtotime($article->date_start)) ?> </div>
				<div class="lieu"> <?=$article->info_pratique; ?> </div>
			</div>
			<?=$article->text?></div>
		</div>
	</article>
<?php endforeach;  ?>

<?php include('includes/foot.php') ?>
