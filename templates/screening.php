<?php include('includes/head.php'); ?>
<article id="<?=$page->slug?>">
	<div class="article-header">
		<h1> <?=$page->title?> </h1> /
		<div class="article-date_start"> <?=$page->date_start?> </div> -
		<div class="article-date-end"> <?=$page->date_end?> </div>
	</div>
	<div class="content">
		<?=$page->text?>
	</div>
	<div class="gallerie">
	<?php  foreach($page->photo as $image) {
	$thumb = $image->size(400, 400); ?>
		<img src='<?=$thumb->url?>' alt='<?=$image->description?>'>
	<?php } ?>
	</div>
	</article>

<?php include('includes/foot.php'); ?>
