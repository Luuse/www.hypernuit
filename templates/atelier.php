<?php include('includes/head.php'); ?>
<div id="slider"></div>
<div id="viewer_pdf"></div>
<article class="atelier" data-date-start="<?=$page->date_start?>" data-date-end="<?=$page->date_end?>" data-category="<?=$page->parent->name?>">

	<div class="home_article_title">
		<div class="<?=$page->parent->name?>">
			<div class="sticky"><?=$page->title?></div>
		</div>
	</div>

<div class="content">
	<div class="info_technique">
		<span class="section">Atelier</span><br>
		<p><?=$page->info_technique?></p>
	</div>

	<?php if($page->photo->count != 0) { ?>
	<div class="gallerie scroll_h">
		<div class="scroll_left"></div>
		<div class="scroll_right"></div>
		<div class="images">
			<?php  foreach($page->photo as $image) {
			$thumb = $image->size(400, 300);
			$mobile = $image->size(1000);
?>
			<img src='<?=$thumb->url?>' data-hight="<?=$mobile->url?>" alt='<?=$image->description?>'/>
			<?php } ?>
		</div>
	</div>
	<?php } ?>

		<div class="texte">

			<div class="info_technique-resp"><span class="section">Atelier</span><br><p><?=$page->info_technique?></p></div>


			<div class="intention">
				<span class="subtitle">FR :</span>
				<span><?=$page->text_fr?></span>
			</div>

			<div class="intention">
				<span class="subtitle">EN :</span>
				<span> <?=$page->text_en?> </span>
			</div>

			<div class="column_right">
				<?php if($page->pdf->count > 0) { ?>
				<div class="files">
				<u class="subtitle"><?=$page->titre_fichier_pdf?> :</u>
					<ul>
					<?php foreach($page->pdf as $pdf) { ?>
					<li class="file_pdf" data-url="<?=$pdf->url?>"><span class="file_name"><?=$pdf->name?></span></li>
					<?php	} ?>
					</ul>
					<br>
				</div>
				<?php	} ?>










			<div class="credits">
				<?php if ($page->credits[0]) { ?>
				<u class="subtitle">CREDITS :</u>
				<div class="peoples">
					<?php foreach($page->credits as $roles) { ?>
					<div class="role"><?=$roles->title?></div>
						<?php	foreach($roles->peoples_repeater as $people) {
					?>
					<a href="<?=$people->peoples[0]->url?>/?about=false" class="personne">
						<div class="nom"><?=$people->peoples[0]->title?>&nbsp;</div>
					</a>
					<?php	} ?>
					<?php	} ?>
					<div class="credits_libre"><?=$page->credits_libre?></div>
				</div>
				<?php } ?>
			</div>




				<?php if ($page->rel_films_install[0]) { ?>
				<div class="relation">
					<span class="subtitle">RELATED PROJECTS :</span>
					<?php foreach($page->rel_films_install as $film) { ?>
					<a href="<?=$film->url?>/?about=false" class="films-installations">
						<div class="nom"> &nbsp;<?=$film->title?></div>
					</a>
					<?php	} ?>
					<?php  } ?>
				</div>
			</div>

		</div>
</div>

</article>

<?php include('includes/foot.php'); ?>
