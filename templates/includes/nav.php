<nav>
	<ul id="nav-list">
		<a href="<?=$home->url?>?about=false">
			<img class="logo" src="<?php echo $config->urls->templates ?>assets/images/logo.svg" alt="">
		</a>

		<details class="part-resp">
		  <summary>colors</summary>
			<div class="nav-part-resp">
				<?php foreach($home->children as $part):?>
				<li class="<?=$part->name?><?=($page->title == $part->title) ? "select" : "";?>" onClick="sortCategory('<?=$part->name?>')">
						<?=$part->color_title?>
				</li>
				<?php endforeach; ?>
			</div>
		</details>

		<div class="part">
			<?php foreach($home->children as $part):?>
			<li class="nav-part <?=$part->name?><?=($page->title == $part->title) ? "select" : "";?>" onClick="sortCategory('<?=$part->name?>')">
					<?=$part->color_title?>
			</li>
			<?php endforeach; ?>
		</div>

		<ul class="nav-right">
			<?php $people = $pages->get('template=peoples'); ?>
			<li class="nav-part <?=$people->name?> " onClick="sortCategory('<?=$people->name?>')">
					<?=$people->color_title?>
			</li>
			<li class="nav-part about"> <a onClick="openAbout()" > about </a>
			</li>
			<li class="nav-part goodies">
				<a href="<?=$home->url?>/goodies/?about=false">
					<img class="picto-goodies" src="<?php echo $config->urls->templates ?>assets/images/goodies.svg" alt="">
				</a>
			</li>
		</ul>
	</ul>
</nav>
