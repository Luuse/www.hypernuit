<?php
include('resetpasswd.php');
$home = $pages->get('template=home');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<style>
			:root {
				<?php foreach($home->children as $part):?>
					 --color-<?=$part->name?> : #<?=$part->color_picker?>;
				<?php endforeach; ?>
			}
		</style>
		<title>hypernuit</title>
  <link id="favicon" rel="icon" type="image/x-icon" href="<?php echo $config->urls->templates ?>/assets/favicon/">
	<link  rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>assets/css/main.css" />

</head>
<body data-home-url="<?=$home->url?>">

<!-- <div class="time-line-main"> -->

<!-- </div> -->
<?php include('nav.php') ?>
 <main class="<?=$page->template->name?>">
	<section id="about" class="visible">
		<div class="cross" onClick="closeAbout()">×</div>
		<div class="content">
			<?php
					$logo = '<img class="logo" src="'.$config->urls->templates.'assets/images/logo-white.svg" alt="">';
					$text = str_replace('[[hypernuit]]', $logo, $home->text );
					echo $text;
			?>
				<br><br>
		</div>
	</section>
	<section id="page">
