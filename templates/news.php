<?php include('includes/head.php') ?>

<?php foreach($page->children as $article):
?>
<article id="<?=$article->slug?>">
		<a href="<?=$article->name?>">
			<div class="article-header">
				<h1> <?=$article->title?> </h1> /
				<div class="article-date"> <?=$article->date_end; ?> </div>
			</div>
      <?php
      if($article->photo->first){
        $thumb = $article->photo->first->size(400);?>
  			<img src="<?=$thumb->url?>" />
      <?php } ?>
		</a>
	</article>
<?php endforeach;  ?>

<?php include('includes/foot.php') ?>
