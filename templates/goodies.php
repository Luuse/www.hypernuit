<?php include('includes/head.php') ?>
<div id="slider"></div>
<div class="goodies-list">

	<?php foreach($page->children as $article):?>
	<article class="<?=$page->name?>" id="<?=$article->name?>">
				<div class="article-header">
					<h1><?=$article->title?> </h1>
				</div>
			<div class="content">
			<div class="col_left">
				<div class="gallerie">
					<?php  foreach($article->photo as $image) {
					$thumb = $image->size(300); ?>
					<img src='<?=$thumb->url?>' data-hight="<?=$image->url?>" alt='<?=$image->description?>'/><?php } ?>
				</div>
				<div class="mention">
					<p><?=$page->text_en?></p>
					<br>
					<p><?=$page->text_fr?></p>
				</div>
			</div>
			<div class="texte">
				<div class="info_pratique">
					<?=$article->text_en?>
					<br>
					<a class="btn_paypal" target="_blank" href="<?=$article->link?>">buy</a>
				</div>

			<div class="relation">

				<u class="subtitle">RELATED PROJECTS :</u>
				<?php if ($article->rel_films_install[0]) { ?>
					<?php  foreach($article->rel_films_install as $film_install) { ?>
					<a href="<?=$film_install->url?>/?about=false" class="films-installations">
						<div class="nom"> &nbsp;<?=$film_install->title?></div>
					</a>
					<?php	} ?>
				<?php } ?>

				<?php if ($page->ref_ateliers[0]) { ?>
					<?php  foreach($page->ref_ateliers as $atelier) { ?>
					<a href="<?=$atelier->url?>/?about=false" class="atelier">
						<div class="nom"> &nbsp;<?=$atelier->title?></div>
					</a>
					<?php	} ?>
				<?php } ?>

				<?php if ($page->ref_sounds[0]) { ?>
				<?php foreach($page->ref_sounds as $sound) { ?>
					<a href="<?=$sound->url?>/?about=false" class="sound">
						<div class="nom">&nbsp;<?=$sound->title?></div>
					</a>
				<?php	} ?>
				<?php } ?>
			</div>

		</div>
			</div>
		</article>
	<?php endforeach;  ?>

</div>
<?php include('includes/foot.php') ?>
