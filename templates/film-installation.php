<?php include('includes/head.php'); ?>

<div id="slider"></div>
<div id="viewer_pdf"></div>

<article class="film_install" data-date-start="<?=$page->date_start?>" data-date-end="<?=$page->date_end?>" data-category="<?=$page->parent->name?>">
	<div class="home_article_title">
		<div class="<?=$page->parent->name?>">
			<div class="sticky">
					<?=$page->title?>

			</div>
		</div>
		<div class="player-light">
			<audio id="player_audio">
				<source src="<?=$page->son->url?>" type="audio/mpeg">
			</audio>
			<div id="btn_play" class="btn_play pause" data-son="<?=$page->son->url?>" onClick='play_sound()'></div>
		</div>
	</div>


	<div class="content">

		<div class="texte">

			<div class="info_technique"><span class="section">
				<?php if($page->type_film == 1) echo 'Film'; ?>
				<?php if($page->type_installation == 1) echo 'Installation'; ?>
				</span><br><?=$page->info_technique?>
			</div>
			<div class="intention">
				<span class="subtitle">FR :</span>
				<span><?=$page->text_fr?></span>
			</div>
			<div class="intention">
				<span class="subtitle">EN :</span>
				<span> <?=$page->text_en?> </span>
			</div>

			<div class="gallerie-resp">
				<?php  foreach($page->photo as $image) {
				$thumb = $image->size(400, 300);
				$mobile = $image->size(400);
?>
					<img src='<?=$thumb->url?>' data-hight="<?=$mobile->url?>" alt='<?=$image->description?>'/>
				<?php } ?>
			</div>

			<?php if($page->pdf->count > 0) { ?>
			<div class="files">
			<u class="subtitle"><?=$page->titre_fichier_pdf?> :</u>
				<ul>
				<?php foreach($page->pdf as $pdf) { ?>
				<li class="file_pdf" data-url="<?=$pdf->url?>"><span class="file_name"><?=$pdf->name?></span></li>
				<?php	} ?>
				</ul>
			</div>
			<?php	} ?>

			<div class="credits">
				<?php if ($page->credits[0]) { ?>
				<u class="subtitle">CREDITS :</u>
				<div class="peoples">
					<?php foreach($page->credits as $roles) { ?>
					<div class="role"><?=$roles->title?></div>
						<?php	foreach($roles->peoples_repeater as $people) {
					?>
					<a href="<?=$people->peoples[0]->url?>/?about=false" class="personne">
						<div class="nom"><?=$people->peoples[0]->title?>&nbsp;</div>
					</a>
					<?php	} ?>
					<?php	} ?>
					<div class="credits_libre"><?=$page->credits_libre?></div>
				</div>
				<?php } ?>
			</div>

			<?php if($page->expositions->count > 0) { ?>
			<div class="places">
				<u class="subtitle"><?=$page->title?> has been visible at this places  :</u>
				<ul>
					<?php foreach($page->expositions as $exposition) { ?>
					<li class="exposition">
						<?php if($exposition->link != ''){ ?>
						<a target="bank" href="<?=$exposition->link?>" ><?=$exposition->title?></a>
						<?php } else { ?>
						<span><?=$exposition->title?></span>
						<?php } ?>
					</li>
					<?php	} ?>
				</ul>
			</div>
			<?php	} ?>

			<?php if(
					 $page->ref_ateliers[0] != '' ||
					 $page->ref_sounds[0] != '' ||
					 $page->rel_films_install[0] != ''
				){ ?>

				<div class="relation">

					<u class="subtitle">RELATED PROJECTS :</u>
					<?php if ($page->ref_ateliers[0]) { ?>
					<?php  foreach($page->ref_ateliers as $atelier) { ?>
					<a href="<?=$atelier->url?>/?about=false" class="atelier">
						<div class="nom"> &nbsp;<?=$atelier->title?></div>
					</a>
					<?php	} ?>
					<?php } ?>
					<?php if ($page->ref_sounds[0]) { ?>
					<?php foreach($page->ref_sounds as $sound) { ?>
					<a href="<?=$sound->url?>/?about=false" class="sound">
						<div class="nom">&nbsp;<?=$sound->title?></div>
					</a>
					<?php	} ?>
					<?php } ?>
					<?php if ($page->ref_goodies[0]) { ?>
					<?php foreach($page->ref_goodies as $goodie) { ?>
					<a href="<?=$goodie->parent->url?>/?about=false" class="goodie">
						<div class="nom">&nbsp;<?=$goodie->title?></div>
					</a>
					<?php	} ?>
					<?php } ?>
				</div>
				<?php } ?>

		</div>

		<div class="gallerie">
			<?php  foreach($page->photo as $image) {
				$thumb = $image->size(400, 300); ?>
				<img src='<?=$thumb->url?>' data-hight="<?=$image->url?>" alt="<?=$image->description?>"/>
			<?php } ?>
		</div>

	</div>

</article>

<?php include('includes/foot.php'); ?>
