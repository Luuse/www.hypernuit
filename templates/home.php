<?php include('includes/head.php') ?>


<?php
  $parts = $page->children;
  foreach ($parts as $part) {
	if ($part->name == "films-installations" ||
			$part->name == "ateliers" ||
			$part->name == "sounds" ||
			$part->name == "screenings" ||
			$part->name == "peoples"
			){
    $articles = $part->children;
      foreach ($articles as $article) {
			if ($article->part !== 'peoples' && ($article->text_fr !== '' || $article->text_en !== '' )) {
			
?>
<?php if (
$part->name == 'screenings'
) { ?>
<a href="<?=$part->name?>/?about=false#<?=$article->name?>">
	<?php }else{?>
	<a href="<?=$part->name?>/<?=$article->name?>/?about=false">
		<?php } ?>
		<article class="item <?=$part->name?>" data-date-start="<?=$article->date_start?>" data-date-end="<?=$article->date_end?>" data-category="<?=$part->name?>" data-published="<?=$article->published?>"
			<?php if($article->image_home) {
				$thumb = $article->image_home->size(400, 300);
			}elseif( $part->name == 'peoples' ) {
				$thumb = $article->photo->first;
			}
			?>
			data-image="<?= $thumb->url ?>"
			<?php   ?>
			>
			<span class="category none" style="visibility: hidden"><?=$part->name?></span>
			<h1 style="visibility: hidden"><?=$article->title?></h1>
		</article>
	</a>
	<?php
			}
			}
					} else if ( $part->name == 'news' ) {
					?>
					<div class="home_article_title"></div>

					<section id="news" >
						<marquee class="marquee_news" behavior="scroll" direction="left" loop="-1" onmouseover="this.stop();" onmouseout="this.start();">
						<?php $articlesNews = $part->children;
						foreach ($articlesNews as $articleNew) { ?>
						<span class="item <?=$part->name?>">
							<a target="_blank" href="<?=$articleNew->link?>">
								<span class="category"><?=$articleNew->date_start?></span> : <?=$articleNew->title?>
							</a>
						</span>
						<?php } ?>
						</marquee>
					</section>

					<!-- <div class="degrade"> -->
					<details class="news-resp">
						<summary>news</summary>
						<div class="items-news-resp">
							<?php $articlesNews = $part->children;
							foreach ($articlesNews as $articleNew) { ?>
             <li class="item<?=$part->name?>">
						 <a target="_blank" href="<?=$articleNew->link?>">
  					   	<span class="category"><?=$articleNew->date_start?></span> : <?=$articleNew->title?>
  					   </a>
        	   </li>
        <?php } ?>
      </div>
  		</details>
    <!-- </div> -->
		<?php
    }
  }
?>

<?php include('includes/foot-home.php') ?>
