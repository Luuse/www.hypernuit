<?php include('includes/head.php'); ?>

<div id="slider"></div>
<div id="viewer_pdf"></div>

<article class="sound" data-date-start="<?=$page->date_start?>" data-date-end="<?=$page->date_end?>" data-category="<?=$page->parent->name?>">
	<div class="home_article_title">
		<div class="<?=$page->parent->name?>">
		<div class="sticky"><?=$page->title?></div>
		</div>
	</div>

	<div class="content">
		<div class="texte">
			<div class="info_technique"><span class="section">Sound</span><br><p><?=$page->info_technique?></p></div>
			<div class="intention">
				<span class="subtitle">FR :</span>
				<span><?=$page->text_fr?></span>
			</div>
			<div class="intention">
				<span class="subtitle">EN :</span>
				<span> <?=$page->text_en?> </span>
			</div>
			<div class="gallerie">
				<?php  foreach($page->photo as $image) {
				$thumb = $image->size(300, 300); ?>
				<img src='<?=$thumb->url?>' data-hight="<?=$image->url?>" alt='<?=$image->description?>'/><?php } ?>
			</div>
			<div class="gallerie-resp">
				<?php  foreach($page->photo as $image) {
				$thumb = $image->size(400, 300);
				$mobile = $image->size(400);

?>
					<img src='<?=$thumb->url?>' data-hight="<?=$mobile->url?>" alt='<?=$image->description?>'/>
				<?php } ?>
			</div>

			<?php if($page->pdf[0]) { ?>
			<div class="files">
				<u class="subtitle">
					FILES :
				</u>
				<ul>
					<?php  foreach($page->pdf as $pdf) { ?>
					<li class="file_pdf" data-url="<?=$pdf->url?>"><?=$pdf->name?></li>
					<?php	} ?>
				</ul>
			</div>
			<?php	} ?>

			<div class="credits-resp">
				<?php if ($page->credits[0]) { ?>
				<u class="subtitle">CREDITS :</u>
				<div class="peoples">
					<?php foreach($page->credits as $roles) { ?>
					<div class="role"><?=$roles->title?></div>
						<?php	foreach($roles->peoples_repeater as $people) {
					?>
					<a href="<?=$people->peoples[0]->url?>/?about=false" class="personne">
						<div class="nom"><?=$people->peoples[0]->title?>&nbsp;</div>
					</a>
					<?php	} ?>
					<?php	} ?>
					<div class="credits_libre"><?=$page->credits_libre?></div>
				</div>
				<?php } ?>
			</div>
			<?php if($page->expositions->count > 0) { ?>
			<div class="places-resp">
				<u class="subtitle"><?=$page->title?> has been listened at this places  :</u>
				<ul>
					<?php foreach($page->expositions as $exposition) { ?>
					<li class="exposition">
						<?php if($exposition->link != ''){ ?>
						<a target="bank" href="<?=$exposition->link?>" ><?=$exposition->title?></a>
						<?php } else { ?>
						<span><?=$exposition->title?></span>
						<?php } ?>
					</li>
					<?php	} ?>
				</ul>
			</div>
			<?php	} ?>
			<?php if(
					 $page->ref_ateliers[0] != '' ||
					 $page->ref_sounds[0] != '' ||
					 $page->rel_films_install[0] != ''
				){ ?>

			<div class="relation-resp">
				<u class="subtitle">RELATED PROJECTS :</u>
				<?php if ($page->rel_films_install[0]) { ?>
				<?php  foreach($page->rel_films_install as $film) { ?>
				<a href="<?=$film->url?>/?about=false" class="films-installations">
					<div class="nom"> &nbsp;<?=$film->title?></div>
				</a>
				<?php	} ?>
				<?php	} ?>
				<?php if ($page->ref_ateliers[0]) { ?>
				<?php  foreach($page->ref_ateliers as $atelier) { ?>
				<a href="<?=$atelier->url?>/?about=false" class="atelier">
					<div class="nom"> &nbsp;<?=$atelier->title?></div>
				</a>
				<?php	} ?>
				<?php  } ?>
				<?php if ($page->ref_sounds[0]) { ?>
				<?php  foreach($page->ref_sounds as $sound) { ?>
				<a href="<?=$sound->url?>/?about=false" class="sound">
					<div class="nom"> &nbsp;<?=$sound->title?></div>
				</a>
				<?php	} ?>
				<?php  } ?>
				<?php if ($page->rel_films_install[0]) { ?>
				<?php  foreach($page->ref_sounds as $sound) { ?>
				<a href="<?=$sound->url?>/?about=false" class="film_install">
					<div class="nom"> &nbsp;<?=$sound->title?></div>
				</a>
				<?php	} ?>
				<?php  } ?>
			</div>
			<?php  } ?>

		</div>

		<div class="col_right">
			<?php if($page->pdf->count > 0) { ?>
			<div class="files">
			<u class="subtitle"><?=$page->titre_fichier_pdf?> :</u>
				<ul>
				<?php foreach($page->pdf as $pdf) { ?>
				<li class="file_pdf" data-url="<?=$pdf->url?>"><span class="file_name"><?=$pdf->name?></span></li>
				<?php	} ?>
				</ul>
			</div>
			<?php	} ?>
			<div class="credits">
				<?php if ($page->credits[0]) { ?>
				<u class="subtitle">CREDITS :</u>
				<div class="peoples">
					<?php foreach($page->credits as $roles) { ?>
					<div class="role"><?=$roles->title?></div>
						<?php	foreach($roles->peoples_repeater as $people) {
					?>
					<a href="<?=$people->peoples[0]->url?>/?about=false" class="personne">
						<div class="nom"><?=$people->peoples[0]->title?>&nbsp;</div>
					</a>
					<?php	} ?>
					<?php	} ?>
					<div class="credits_libre"><?=$page->credits_libre?></div>
				</div>
				<?php } ?>
			</div>
			<?php if($page->expositions->count > 0) { ?>
			<div class="places">
				<u class="subtitle"><?=$page->title?> has been listened at this places  :</u>
				<ul>
					<?php foreach($page->expositions as $exposition) { ?>
					<li class="exposition">
						<?php if($exposition->link != ''){ ?>
						<a target="bank" href="<?=$exposition->link?>" ><?=$exposition->title?></a>
						<?php } else { ?>
						<span><?=$exposition->title?></span>
						<?php } ?>
					</li>
					<?php	} ?>
				</ul>
			</div>
			<?php	} ?>

			<?php if(
					 $page->ref_ateliers[0] != '' ||
					 $page->ref_sounds[0] != '' ||
					 $page->rel_films_install[0] != ''
				){ ?>

				<div class="relation">

					<u class="subtitle">RELATED PROJECTS :</u>
					<?php if ($page->rel_films_install[0]) { ?>
					<?php  foreach($page->rel_films_install as $film) { ?>
					<a href="<?=$film->url?>/?about=false" class="films-installations">
						<div class="nom"> &nbsp;<?=$film->title?></div>
					</a>
					<?php	} ?>
					<?php	} ?>
					<?php if ($page->ref_ateliers[0]) { ?>
					<?php  foreach($page->ref_ateliers as $atelier) { ?>
					<a href="<?=$atelier->url?>/?about=false" class="atelier">
						<div class="nom"> &nbsp;<?=$atelier->title?></div>
					</a>
					<?php	} ?>
					<?php } ?>
					<?php if ($page->ref_sounds[0]) { ?>
					<?php foreach($page->ref_sounds as $sound) { ?>
					<a href="<?=$sound->url?>/?about=false" class="sound">
						<div class="nom">&nbsp;<?=$sound->title?></div>
					</a>
					<?php	} ?>
					<?php } ?>
					<?php if ($page->ref_goodies[0]) { ?>
					<?php foreach($page->ref_goodies as $goodie) { ?>
					<a href="<?=$goodie->parent->url?>/?about=false" class="goodie">
						<div class="nom">&nbsp;<?=$goodie->title?></div>
					</a>
					<?php	} ?>
					<?php } ?>
				</div>
				<?php } ?>

		</div>

	</div>


	<?php if($page->son){ ?>

	<div class="player">
		<audio id="player_source" controls="">
			<source id="player_source_item" src="<?=$page->son->url ?>" type="audio/mp3">
		</audio>
		<div class="audio-player">
			<div class="timeline">
				<div class="progress" style="width: 0%;"></div>
			</div>
			<div class="controls">
				<div class="play-container">
					<div class="toggle-play play">
					</div>
				</div>
				<div class="volume-container">
					<div class="volume-button">
						<div class="volume icono-volumeMedium"></div>
					</div>
					<div class="volume-slider">
						<div class="volume-percentage"></div>
					</div>
				</div>
				<div class="name"><?=$page->title?></div>
				<div class="time">
					<div class="current">0:00</div>
					<div class="divider">/</div>
					<div class="length">0:00</div>
				</div>
				<!-- <div id="btn_prev_next"> -->
				<!-- 	<span class="prev actif">&#38;lt; previous </span> | -->
				<!-- 	<span class="next delete_audio actif"> next &#38;gt; </span> -->
				<!-- </div> -->
			</div>
		</div>
	</div>
	<?php } ?>




</article>

<?php include('includes/foot.php'); ?>
