from moon.terminal_ui import TerminalUi

ui = TerminalUi()
ui.set_moon_phase() #defaults to today's date
print(ui.moon_datetime_info)
ui.show()

